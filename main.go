/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "github.com/rdlsolutions/hobot/cmd"

func main() {
	cmd.Execute()
}
